# @vizworx/babel-preset

`@vizworx/babel-preset` is a combination of [`@babel/preset-env`](https://babeljs.io/docs/en/babel-preset-env), [`@babel/preset-react`](https://babeljs.io/docs/en/babel-preset-react), and plugins that are commonly used at [VizworX](https://vizworx.com).

## Install

With npm:

```sh
npm install --save-dev @vizworx/babel-preset
```

Or yarn:

```sh
yarn add @vizworx/babel-preset --dev
```

## Usage

### .babelrc

```json
{
  "presets": ["@vizworx/babel-preset"]
}
```

#### Preset Options

Some projects may need to customize the underlying Babel configuration. For this purpose, `@vizworx/babel-preset` can accept options using the appropriate preset as a key and pass them along:

```json
"presets": [
  [
    "@vizworx/babel-preset",
    {
      "@babel/preset-env": {
        "targets": {
          "node": "current"
        }
      }
    }
  ]
]
```

## Plugins

In addition to the plugins used by [`@babel/preset-env`](https://babeljs.io/docs/en/babel-preset-env) and [`@babel/preset-react`](https://babeljs.io/docs/en/babel-preset-react), we have chosen to enable some additional plugins that simplify development. Many of these plugins are for [TC39 proposals](https://github.com/tc39/proposals) that are in either Stage 3 (Candidate) or Stage 4 (Finished) of the [TC39 Process](https://tc39.es/process-document/), which are both considered to be unlikely to change before the next ECMAScript release.

For an explanation of the TC39 stages, and why we feel comfortable using proposals that have reached Stage 3, please read the following articles by Yehuda Katz:

* https://thefeedbackloop.xyz/tc39-a-process-sketch-stages-0-and-1/
* https://thefeedbackloop.xyz/tc39-process-sketch-stage-2/
* https://thefeedbackloop.xyz/tc39-process-sketch-stage-3/

| Plugin | TC39 Proposal | TC39 Stage | Reason |
| ------ | ------------- | ---------- | ------ |
| [Object Rest/Spread Properties](https://babeljs.io/docs/plugins/transform-object-rest-spread/) | [proposal-object-rest-spread](https://github.com/tc39/proposal-object-rest-spread) | 4 | Spreading and destructuring objects and arrays is a common pattern in React |
| [Class Properties](https://babeljs.io/docs/en/babel-plugin-proposal-class-properties) | [proposal-static-class-features](https://github.com/tc39/proposal-class-fields)<br />[proposal-class-fields](https://github.com/tc39/proposal-static-class-features) | 3 | Class instance and static properties/methods are a recommended way of writing advanced React components |
| [Nullish Coalescing Operator](https://babeljs.io/docs/en/babel-plugin-proposal-nullish-coalescing-operator) | [proposal-nullish-coalescing](https://github.com/tc39/proposal-nullish-coalescing) | 3 | Adds an operator of `??` that is similar to `\|\|` but only applies to `undefined` and `null`, instead of all falsey values. This can be used to default missing values, and is extremely powerful when combined with Optional Chaining<br />Falsey with empty string: `const value = '' \|\| 'example'; // value is 'example'`<br />Nullish with empty string: `const value = '' ?? 'example'; // value is ''`<br />Nullish with null: `const value = null ?? 'example'; // value is 'example'` |
| [Optional Chaining](https://babeljs.io/docs/en/babel-plugin-proposal-optional-chaining) | [proposal-optional-chaining](https://github.com/tc39/proposal-optional-chaining) | 3 | Optional Chaining builds upon the `.` operator in objects (`user.profile.id`) with a new `?.` operator that will safely handle null/undefined values. When combined with Nullish Coalescing Operators, this simplifies retrieving optional data with a default value.<br />Old: `let name = 'Guest'; if (user && user.profile && user.profile.name) { name = user.profile.name; }`<br />New `const name = user?.profile?.name ?? 'Guest';` |
| [Syntax Dynamic Import](https://babeljs.io/docs/en/babel-plugin-syntax-dynamic-import) | N/A | N/A | Adds support for dynamic imports and code splitting via [Webpack](https://webpack.js.org/guides/code-splitting/#dynamic-imports) |

Current proposals can be viewed at the [TC39 Proposals repository](https://github.com/tc39/proposals) with a list of [Finished Proposals](https://github.com/tc39/proposals/blob/master/finished-proposals.md)
