module.exports = function(_, options = {}) {
  return {
    presets: [
      [
        require.resolve('@babel/preset-env'),
        {
          useBuiltIns: 'usage',
          corejs: 3,
          ...options['@babel/preset-env'],
        },
      ],
      [
        require.resolve('@babel/preset-react'),
        {
          ...options['@babel/preset-react'],
        },
      ]
    ],
    plugins: [
			//
			// TC39 Proposals
			//
      // Stage 4: https://github.com/tc39/proposal-object-rest-spread
      require.resolve('@babel/plugin-proposal-object-rest-spread'),
      // Stage 3: https://github.com/tc39/proposal-class-fields
      // Stage 3: https://github.com/tc39/proposal-static-class-features
      require.resolve('@babel/plugin-proposal-class-properties'),
      // Stage 3: https://github.com/tc39/proposal-nullish-coalescing
      require.resolve('@babel/plugin-proposal-nullish-coalescing-operator'),
      // Stage 3: https://github.com/tc39/proposal-optional-chaining
      require.resolve('@babel/plugin-proposal-optional-chaining'),

      //
      // Syntax support for Webpack
      //
      // Adds support for dynamic imports and code splitting
      // https://webpack.js.org/guides/code-splitting/#dynamic-imports
      // https://babeljs.io/docs/en/babel-plugin-syntax-dynamic-import
      require.resolve('@babel/plugin-syntax-dynamic-import'),
    ],
  };
}
